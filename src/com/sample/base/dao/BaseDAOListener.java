package com.sample.base.dao;

public interface BaseDAOListener {
	abstract void onDataLoaded(BaseDAO dao);
	abstract void onDataFailed(BaseDAO dao);
}
