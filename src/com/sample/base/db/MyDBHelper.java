package com.sample.base.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 *数据库的定义类,各个操作类在com.smaple.db中定义
 */
public class MyDBHelper extends SQLiteOpenHelper{
	private static final String DATABASE_NAME="kuaiditu_user.db";//只建一个数据库
	private static final int VERSION=1;//如果修改了数据库，记得修改版本号,否则老版本更新时无法更新数据

	//城市表和字段
	public static final String TABLE_CITY="table_city";//存储省市区的表
	public static final String CITY_REGION_ID="region_id";//区域id
	public static final String CITY_USER_ID="user_id";//用户id
	public static final String CITY_REGION_NAME="region_name";//区域名称
	public static final String CITY_LEVEL="level";//级别 省市区
	public static final String CITY_PARENT_ID="parent_id";
	public static final String CITY_DIPLAY_ORDER="display_order";
	public static final String CITY_ENABLE="enabled";
	public static final String CITY_SEQUENCE="sequence";

	public MyDBHelper(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sqlCity = "CREATE TABLE "
				+TABLE_CITY+"("
				+CITY_REGION_ID+" INTEGER PRIMARY KEY,"
				+CITY_USER_ID+" INTEGER,"
				+CITY_REGION_NAME+" NVARCHAR(12),"
				+CITY_LEVEL+" INTEGER,"
				+CITY_PARENT_ID+" INTEGER,"
				+CITY_DIPLAY_ORDER+" INTEGER,"
				+CITY_SEQUENCE+" INTEGER,"
				+CITY_ENABLE+" INTEGER)";
		db.execSQL(sqlCity);
	}
	@Override
	public SQLiteDatabase getWritableDatabase() {
		return super.getWritableDatabase();
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists " + TABLE_CITY);
		onCreate(db);
	}

}
