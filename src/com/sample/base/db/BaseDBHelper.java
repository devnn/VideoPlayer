package com.sample.base.db;

import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * 数据库操作基类
 */
public abstract class BaseDBHelper<T> {

	protected String TAG=this.getClass().getSimpleName();

	protected SQLiteDatabase sqliteDatabase;

	public MyDBHelper myDBHelper;

	public BaseDBHelper(Context context){
		this.myDBHelper=new MyDBHelper(context);
	}



	/**
	 * 打开数据库 增删改查前必须要调用 
	 */
	protected void openDB(){
		this.sqliteDatabase=myDBHelper.getWritableDatabase();
	}



	/**
	 * 关闭数据库 增删改查后必须要调用 
	 */
	protected void closeDB(){
		if(sqliteDatabase!=null){
			if(this.sqliteDatabase.isOpen()){
				this.sqliteDatabase.close();
			}
		}
	}



	/**
	 * 插入数据
	 * @param userId 当前登录的用户id
	 * @param object 要插入的数据对象
	 * @return
	 */
	public abstract  long insert(int userId,T object);



	/**
	 * 插入数据
	 * @param userId 当前登录的用户id
	 * @param object 要插入的数据对象集合
	 * @return 受影响的行数
	 */
	public abstract long insert(int userId,List<T> objects);



	/**
	 * 更新数据
	 * @param userId 当前登录的用户id
	 * @param object 要更新的数据对象
	 * @return 受影响的行数
	 */
	public abstract int update(int userId,T object);



	/**
	 * 删除一条数据
	 * @param userId 当前登录的用户id
	 * @param dataId 要删除的数据的id
	 * @return 受影响的行数
	 */
	public abstract int delete(int userId,long dataId);


	/**
	 * 删除当前用户的所有数据
	 * @param userId
	 * @return
	 */
	public abstract int deleteAll(int userId);
	
	
	
	/**
	 * 查询当前用户的所有数据
	 * @param userId 当前登录的用户id
	 * @return 查询的结果 返回表中所有数据
	 */
	public abstract List<T> queryAll(int userId);



	/**
	 * 查询一条数据
	 * @param userId 当前登录的用户id
	 * @param dataId 要查询的数据的id
	 * @return 查询的结果 返回一条数据
	 */
	public abstract T query(int userId,long dataId);
	
	
	
	/**
	 * 查询数据量
	 * @param userId 当前登录的用户id
	 * @return 查询的结果
	 */
	public abstract int getSize(int userId);
}
