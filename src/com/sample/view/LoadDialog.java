package com.sample.view;

import com.devnn.videoplayer.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

public class LoadDialog extends Dialog implements View.OnClickListener{
	private TextView tvMessage;
	private Context context;
	public LoadDialog(Context context) {
		super(context,R.style.DialogStyle);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.load_dialog);
		getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		this.getWindow().setLayout(context.getResources().getDisplayMetrics().widthPixels-40, LayoutParams.WRAP_CONTENT);
		initView();
		setEvent();
	}
	private void initView(){
		tvMessage=(TextView)findViewById(R.id.load_dialog_message);
	}
	private void setEvent(){
	}
	public void setMessage(String msg){
		tvMessage.setText(msg);
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}

}
