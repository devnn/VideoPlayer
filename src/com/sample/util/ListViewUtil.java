package com.sample.util;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ListViewUtil {

	public   static void setHeight(ListView list) {
		ListAdapter listAdapter = list.getAdapter();
		if (listAdapter == null) {
			return;
		}
		int totalHeight = 0;
		Log.i("ListViewUtil", "list count:"+listAdapter.getCount());
		for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
			View listItem = listAdapter.getView(i, null, list);
//				 		listItem.measure(LinearLayout.LayoutParams.MATCH_PARENT,0);
			listItem.measure(0,0);
			Log.i("ListViewUtil", "item height:"+listItem.getMeasuredHeight());
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = list.getLayoutParams();
		params.height = totalHeight+ (list.getDividerHeight() * (listAdapter.getCount() - 1));
		list.setLayoutParams(params);
	}
}
