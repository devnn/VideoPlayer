package com.sample.util;

import android.view.MotionEvent;

public class ActionUtil {
	public static String getActionType(MotionEvent event){
		String type="other";
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			type= "ACTION_DOWN";
			break;
		case MotionEvent.ACTION_MOVE:
			type="ACTION_MOVE";
			break;
		case MotionEvent.ACTION_UP:
			type="ACTION_UP";
			break;
		case MotionEvent.ACTION_CANCEL:
			type="ACTION_CANCEL";
			break;
		}
		return type;
	}
}
