package com.devnn.videoplayer;

import com.sample.view.VideoPlayer;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

public class PlayActivity extends BaseActivity implements VideoPlayer.VideoSizeChangedListener{
	private VideoPlayer player;
	private View head;
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		setContentView(R.layout.play_layout);
		super.onCreate(arg0);
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		player=(VideoPlayer) findViewById(R.id.videoplayer);
		player.setVideoSizeChangedListener(this);
		head=findViewById(R.id.head);
	}

	@Override
	protected void setData() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setEvent() {
		// TODO Auto-generated method stub

	}
	private void full(boolean enable) {
		if (enable) {
			player.setSize(false);
			head.setVisibility(View.GONE);
			WindowManager.LayoutParams lp = getWindow().getAttributes();
			lp.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
			getWindow().setAttributes(lp);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		} else {
			player.setSize(true);
			head.setVisibility(View.VISIBLE);
			WindowManager.LayoutParams attr = getWindow().getAttributes();
			attr.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
			getWindow().setAttributes(attr);
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
		}
	}
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation==Configuration.ORIENTATION_PORTRAIT){
			full(false);
			Log.i(TAG, "orintation:portraint");
		}else if(newConfig.orientation==Configuration.ORIENTATION_LANDSCAPE){
			full(true);
			Log.i(TAG, "orintation:landscape");
		}
	}
	@Override
	public void videoSizeChanged(boolean isProtraint) {
		// TODO Auto-generated method stub
		if(isProtraint){
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			full(false);
		}else{
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			full(true);
		}
	}

}
