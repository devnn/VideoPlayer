package com.devnn.videoplayer;

import com.sample.base.dao.BaseDAO;
import com.sample.base.dao.BaseDAOListener;
import com.sample.view.LoadDialog;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public abstract class BaseFragment extends Fragment implements BaseDAOListener,OnClickListener{
	protected String TAG=this.getClass().getSimpleName();
	private  LoadDialog loadDialog=null;
	private View view;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onCreateView()");
		super.onCreateView(inflater, container, savedInstanceState);
		view=inflater.inflate(getLayoutRes(), null);
		init();
		setData();
		setEvent();
		return view;
	}
	protected abstract int getLayoutRes();
	protected abstract void init();
	protected abstract void setData();
	protected abstract void setEvent();
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onResume()");
		super.onResume();
	}
	protected void showLoadDialog(){
		if(loadDialog==null){
			loadDialog=new LoadDialog(this.getActivity());
		}
		loadDialog.show();
	}
	protected void closeLoadDialog(){
		if(loadDialog!=null){
			loadDialog.cancel();
		}
	}
	@Override
	public void onDataLoaded(BaseDAO dao) {
		// TODO Auto-generated method stub
		closeLoadDialog();
	}
	@Override
	public void onDataFailed(BaseDAO dao) {
		// TODO Auto-generated method stub
		closeLoadDialog();
	}

}
