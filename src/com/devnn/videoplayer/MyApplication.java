package com.devnn.videoplayer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.sample.base.dao.BaseRequest;
import com.sample.entity.User;

import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class MyApplication extends Application{
	private String TAG=this.getClass().getSimpleName();
	private static MyApplication application;//通过getInstance()获取实例
	private User user;
	public boolean isAppRunning;
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		isAppRunning=true;
		application=this;
		BaseRequest.configure();
		createFilePath();
		initImageLoader();
		user=recoverUser();
	}
	public static MyApplication getInstance(){
		return application;
	}
	public static boolean isDebug(){
		return (getInstance().getApplicationInfo().flags&android.content.pm.ApplicationInfo.FLAG_DEBUGGABLE)!=0 ? true : false;
	}

	public User getUser(){
		return this.user;
	}
	public void setUser(User user){
		this.user=user;
		saveUser(user);
	}
	
	private void createFilePath(){
		Log.d(TAG, "sd path:"+Environment.getExternalStorageDirectory().getAbsolutePath());
		if(!(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))){
			Log.e(TAG, "no sdcard");
			return;
		}
		File cacheDir=new File(Environment.getExternalStorageDirectory()+File.separator+Config.base_data_path+File.separator+Config.images_cache_path);
		if(!cacheDir.exists()){
			cacheDir.mkdirs();
		}
	}
	private User recoverUser(){
		try {
			FileInputStream fos = this.openFileInput(Config.user_file_name);
			ObjectInputStream oinput=null;
			oinput=new ObjectInputStream(fos);
			User user=(User) oinput.readObject();
			fos.close();
			oinput.close();
			Log.i(TAG, "recover user success!");
			Log.i(TAG, user.toString());
			return user;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e(TAG, "recover user failed!");
		}
		return null;
	}
	public void saveUser(User user){
		try {
			FileOutputStream fos = this.openFileOutput(Config.user_file_name, Context.MODE_PRIVATE);
			ObjectOutputStream oout=null;
			oout=new ObjectOutputStream(fos);
			oout.writeObject(user);
			fos.close();
			oout.close();
			Log.d(TAG, "save user success!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "save user failed!");
			e.printStackTrace();
		} // 
	}
	private void initImageLoader(){
		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.cacheInMemory(true).cacheOnDisk(true)
		.showImageOnFail(R.drawable.ic_launcher)//默认图片
		.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
		.diskCache(new UnlimitedDiscCache(new File(Environment.getExternalStorageDirectory()+File.separator+Config.base_data_path+File.separator+Config.images_cache_path)))
		.defaultDisplayImageOptions(options). // 上面的options对象，一些属性配置
		build();
		ImageLoader.getInstance().init(config); // 初始化
	}
	public void exit(){
		Log.d(TAG, "exit...");
	}
}
