package com.devnn.videoplayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	public void play(View view){
		Intent intent=new Intent();
		intent.setClass(this,PlayActivity.class);
		this.startActivity(intent);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setEvent() {
		// TODO Auto-generated method stub
		
	}
}
