package com.devnn.videoplayer;

import java.util.Timer;
import java.util.TimerTask;
import com.sample.base.dao.BaseDAO;
import com.sample.base.dao.BaseDAOListener;
import com.sample.view.LoadDialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;

public abstract class BaseActivity extends FragmentActivity implements BaseDAOListener,OnClickListener{
	protected String TAG=this.getClass().getSimpleName();
	private  LoadDialog loadDialog=null;
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onCreate()");
		super.onCreate(arg0);
		init();
		setEvent();
		setData();
	}
	public void back(View view){
		finish();
	}
	protected abstract void init();
	protected abstract void setData();
	protected abstract void setEvent();
	protected void showLoadDialog(){
		if(loadDialog==null){
			loadDialog=new LoadDialog(this);
		}
		loadDialog.show();
	}
	protected void closeLoadDialog(){
		if(loadDialog!=null){
			loadDialog.cancel();
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onResume()");
		super.onResume();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	@Override
	public void onDataLoaded(BaseDAO dao) {
		// TODO Auto-generated method stub
		closeLoadDialog();
	}
	@Override
	public void onDataFailed(BaseDAO dao) {
		// TODO Auto-generated method stub
		closeLoadDialog();
	}
	/**
	 * 打开软键盘
	 * @param editView
	 */
	protected void showInputWindow(final View editView){
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			public void run() {
				InputMethodManager inputManager = (InputMethodManager) BaseActivity.this.getSystemService(
								Context.INPUT_METHOD_SERVICE);
				inputManager.showSoftInput(editView, 0);
			}
		}, 500);
	}
	/**
	 * 关闭软键盘
	 */
	protected void closeInputWindow(){
		
	}
	@SuppressWarnings("unchecked")
	protected <T> T $(int viewID) {
	    return (T) findViewById(viewID);
	}
}
